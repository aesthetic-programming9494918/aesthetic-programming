# miniX01

![one example](/miniX01/billede1.png "preview")
![another example](/miniX01/billede2.png "preview")

You can run the program [here](https://aesthetic-programming9494918.gitlab.io/aesthetic-programming/miniX01)

You can view the code [here](https://gitlab.com/aesthetic-programming9494918/aesthetic-programming/-/tree/main/miniX01)

This is a page where I encourage you to draw a person using the stationary eyes provided on the page. You cannot erase parts of your drawing, but you can start afresh by refreshing the page. 
You are also able to draw something else, or nothing at all. 
You draw by pressing the left mouse button while you move the cursor around. 

I had different ideas about what I should write in the heading, and at first, I thought it should be something more personal, like 'draw your thoughts' or something similar. I just couldn't create a sentence that wasn't too long, abstract or pushy. It should be a fun experiece to visit the page, and that wouldn't be the case if you aren't comfortable or capable of drawing what's on your mind. I figured that everyone who would visit this page knows a way to draw a person, even with the obstacle of fitting the pre-made eyes into the drawing. 

I have always loved writing text, which have some similaraties with the coding process, the way that you sit down consentrated and think about the relevance of every sentense, and in what order it should be. Also when I write I often write for others to read it, both with ordinary text and code. 
What I especially like about writing code, that is diffrent from text, is that you can see a result of your writing, while you are still in the process. I really like to make all these small changes along the way, and explore what diffrence they make. This little adventure is something the ordinary text writing can't give me. 

