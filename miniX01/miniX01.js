let img;

function preload() {
  img = loadImage('/miniX01/billede1.png'); 
}

function setup() {
createCanvas(windowWidth,windowHeight);
textSize(32);
fill(255);
stroke(0);
strokeWeight(4);
text('Draw me a person',150,50);
}

function draw() {
  fill(35,0,220)
  stroke(255,0,0)
  ellipse(250,240,50,80)
  ellipse(290,240,50,80)
  fill(255)
  noStroke()
  ellipse(240,240,20,20)
  ellipse(280,240,20,20)

  image(img, width - img.width - 20, 20);
}


function mouseDragged() {
  stroke(0,0,255)
  line(pmouseX, pmouseY, mouseX, mouseY);
}
