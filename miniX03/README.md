# It takes forever

![](/miniX03/ItTakesForever.png "preview")

You can run the program [here](https://aesthetic-programming9494918.gitlab.io/aesthetic-programming/miniX03)

You can view the code [here](https://gitlab.com/aesthetic-programming9494918/aesthetic-programming/-/tree/main/miniX03)

On this page, I aim to illustrate how infinite waiting can feel when waiting for something to load. I've tried to focus on the difference in our perception of time, real time, and computer time.

In the center of the page, there's a throbber representing a large clock. I've set the frameRate to 1, so the hand moves once per second, following real time. Additionally, I've divided the clock so that each quarter represents a season, shown with an emoji on the left. This is done to exaggerate the feeling of it taking ages for it to finish loading. To make the message clear, there is a text that says "it feels like forever", which appears at random positions every second.


When the clock has completed a full rotation, it kinda looks like an hour has passed, as with other clocks, but in reality, only twelve seconds have elapsed. This puts time into perspective, as the computer thinks much faster than we tend to credit it for.


What seasons the emojis represents
- sun: summer
- brown leafs: fall
- snowflake: winter
- sprout: spring

enjoy!
