function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(1); //1 frame per second, the draw() function will be called once every second;
}
 function draw() {
  background(80,150,80,100); //alpha(transparency) value of 100 - you can see the previous drawn frames;
  drawElements();
 }
 function drawElements() {
  let num = 12;
  push();
  translate(width / 2, height / 2); //Moves the origin (0,0) of the canvas to the center;
  //360/num >> degree of each ellipse's movement;
  //frameCount%num >> calculates the remainder when the frame count is divided by the 12 possible positions; 
  //determine which ellipse should be most visible;
  let cir = 360 / num * (frameCount % num);
  rotate(radians(cir)); //Rotates the coordinate system by the calculated angle;
  noStroke();
  fill(255, 255, 0);
  ellipse(0, 80, 5, 170);
  pop();

  if (frameCount % num === 0) {
    // Draw sun when "clock 6" ellipse is most visible;
    textSize(100)
    text('☀️',200,200)
  } else if (frameCount % num === num - 9) {
    // Draw leafs when the "clock 9" ellipse is most visible;
    textSize(100)
    text('🍂',100,300)
  } else if (frameCount % num === num / 2) {
    // Draw snowflake when the "clock 12" ellipse is most visible;
    textSize(100)
    text('❄️',200,400)
  } else if (frameCount % num === num - 3) {
    // Draw sprout when the "clock 3" ellipse is most visible;
    textSize(100)
    text('🌱',100,500)
  }

  for (let i = 0; i < 1; i++) { 
    let x = random(width); //draws text at random positions
    let y = random(height);
    textSize(50);
    textFont('Courier New');
    text('It takes forever', x, y);
  }
}

 function windowResized() { //the clock is still in the middle of the window even if it's resized because they are drawn relative to the center of the canvas;
  resizeCanvas(windowWidth, windowHeight);
 }