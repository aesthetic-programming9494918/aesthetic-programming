# Generative Circle Program
This generative program creates an animated display of circles moving around the canvas, changing colors over time and bouncing off the edges.

![one example](/miniX05/circles.png "preview")

You can run the program [here](https://aesthetic-programming9494918.gitlab.io/aesthetic-programming/miniX05)

You can view the code [here](https://gitlab.com/aesthetic-programming9494918/aesthetic-programming/-/tree/main/miniX05)


## Rules
1. **Change Color Over Time:** Each circle changes its color gradually over time, transitioning from one color to another based on its position on the canvas.
2. **Draw Elements at Random Locations:** Circles are drawn at random locations on the canvas initially and continue to move randomly throughout the animation.

## Program Performance Over Time
The program starts by creating an initial set of circles with random positions, sizes, velocities, and colors. As the draw() function executes repeatedly, the circles move according to their velocities, changing positions and bouncing off the edges when necessary. Additionally, each circle changes its color gradually over time, resulting in a dynamic and visually interesting display.

## Emergent Behavior
The combination of rules governing the movement, color change, and random positioning of the circles results in emergent behavior that is both unpredictable and aesthetically pleasing. Over time, patterns may emerge in the movement of the circles, and interesting color interactions may occur as circles overlap and change color.

## Role of Rules and Processes
Rules and processes play a crucial role in shaping the behavior and appearance of the generative program. By defining specific rules for the movement, color change, and positioning of the circles, we create a structured framework within which emergent patterns and behaviors can arise. These rules provide the program with both direction and constraints, guiding its evolution over time while allowing for the emergence of complex and visually engaging outcomes.

### References
- https://p5js.org/reference/#/p5/alpha
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of
- https://p5js.org/reference/#/p5/map

