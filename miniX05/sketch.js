let circles = [];

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(255);
  
  // Create initial circles
  for (let i = 0; i < 80; i++) {
    let circle = {
      x: random(width),
      y: random(height),
      radius: random(5, 50),
      dx: random(-2, 2), // x velocity
      dy: random(-2, 2), // y velocity
      color: color(random(255), random(255), random(255)) // random color
    };
    circles.push(circle);
  }
}

function draw() {
  background(255);
  
  // Rule 2. draw elements at random locations
  // Move and draw circles
  for (let circle of circles) {
    // Update position
    circle.x += circle.dx;
    circle.y += circle.dy;
    
    // Rule 1: elements should change color over time
    // Draw the circle with changing color
    let alpha = map(circle.x, 0, width, 0, 255); // Change transparency based on x position
    fill(red(circle.color), green(circle.color), blue(circle.color), alpha);
    ellipse(circle.x, circle.y, circle.radius * 2, circle.radius * 2);
    
    // Bounce off the edges
    if (circle.x - circle.radius < 0 || circle.x + circle.radius > width) {
      circle.dx *= -1;
    }
    if (circle.y - circle.radius < 0 || circle.y + circle.radius > height) {
      circle.dy *= -1;
    }
  }
}

