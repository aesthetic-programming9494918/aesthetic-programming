const adviceAPI = 'https://api.adviceslip.com/advice';
let adviceElement; // Declare a variable to hold the advice element


function setup() {
  noCanvas();
let button = select('#GetAdviceButton'); // Select the button element by its ID from the HTML file
button.mousePressed(getAdvice);
}

function getAdvice() { // Function to fetch advice from the API
  let url = adviceAPI; // Construct the URL for fetching advice

  loadJSON(url, gotAdvice); // Load JSON data from the constructed URL and call gotAdvice function when done
}

// Function called when advice data is received from the API
function gotAdvice(data) {
  // Extract the advice from the received data
  let advice = data.slip.advice;

  // Select the advice container element by its ID
  let adviceContainer = select('#advice-container');
  
  // Set the HTML content of the advice container to the received advice
  adviceContainer.html(advice);
  
  // Make the advice container visible
  adviceContainer.show();
}


