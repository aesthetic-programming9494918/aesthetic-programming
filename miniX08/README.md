# Fortune Cookie

![](/miniX08/inAction.mov)

You can run the program [here](https://aesthetic-programming9494918.gitlab.io/aesthetic-programming/miniX08)

You can view the code [here](https://gitlab.com/aesthetic-programming9494918/aesthetic-programming/-/tree/main/miniX08)


## What is the program about? Which API have you used and why?

We've developed a program inspired by fortune cookies, a dessert commonly served in Chinese restaurants in Western countries. Inside each cookie is a piece of paper containing a "fortune," which could be an aphorism or a vague prophecy.

In our program, the background depicts a blurred restaurant scene, with a fortune cookie placed in the center of the window, ready to be clicked on. When clicked, the cookie opens, revealing a randomly generated advice.

To provide these random advices, we used an API that currently distributes over 10 million pieces of advice annually. By integrating this API, we ensured access to a vast reservoir of advice without the need to generate or input them manually.

## Can you describe and reflect on your process in this miniX in terms of acquiring, processing, using, and representing data? How much do you understand this data or what do you want to know more about? How do platform providers sort the data and give you the requested data? What are the power relations in thechosen APIs? What is the significance of APIs in digital culture?

We started the process by searcing for an API that contained data we wanted to display in a way. We then found an API that contained random quotes, and decided to make a fortune cookie button to reveal a random quote fetched from our chosen API. 

Our API is a free JSON API that returns random advice as a slip object. All the quotes has an ID number to keep track of the over 200 different quotes. 

APIs allow for a sort of delegation of tasks in programming and Digital Culture at large.  If there were no APIs every programming team would have to repeat the work of previous programmers over and over again. Because of APIs, something can be made once, and it can be reused, instead of reproduced.

For this project, we chose to use an advice API. When the API is prompted, it gives us life advice, that is very reminiscent of the advice given in fortune cookies. The API handles the creation of this advice, and we handle the presentation and prompting of said advice.

## Try to formulate a question in relation to web APIs or querying/parsing processes that you would like to investigate further if you had more time.

If we had more time, we would like to explore the question of how to avoid errors in the use of external APIs. We experienced, during the work on this miniX, that errors occurred when we in our sketch.js file combined standard p5.js functions such as the draw function with the API URL, and that we had to implement most functions in HTML and styling in CSS to make everything work.

## What is the significance of APIs in Digital Culture?

APIs allow for a sort of delegation of tasks in programming and Digital Culture at large.  If there were no APIs every programming team would have to repeat the work of previous programmers over and over again. Because of APIs, something can be made once, and it can be reused, instead of reproduced.

### The power relations in the chosen APIs?

For this project, we chose to use an advice API. When the API is prompted, it gives us life advice, that is very reminiscent of the advice given in fortune cookies. The API handles the creation of this advice, and we handle the presentation and prompting of said advice.


#### References
- https://api.adviceslip.com/
- https://en.wikipedia.org/wiki/Fortune_cookie
- https://www.scaler.com/topics/image-button-in-html/ 
- https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r
