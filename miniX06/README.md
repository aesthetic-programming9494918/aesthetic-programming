# miniX06 - a Pong game

## Overview
This is a simple implementation of the classic game Pong using p5.js. The game features two paddles controlled by the players, a ball bouncing between the paddles, and a scoring system. The game ends when one player reaches a score of 5.

![](/PONGgame.png "preview")

You can run the program [here](https://aesthetic-programming9494918.gitlab.io/aesthetic-programming/miniX06)

You can view the code [here](https://gitlab.com/aesthetic-programming9494918/aesthetic-programming/-/tree/main/miniX06)

## personal motivation
I knew that I had to make a multiplayer game, as all of my favoritegames through the years have been multiplayergames, both on Playstation and PC. So that was my main criteria for the game. I also preferred it to be a competition, so it has to has some kind of a point system.

## objects 
### Ball
The Ball class represents the ball in the game. It has properties such as x and y coordinates, width, height, and speed. The ball moves according to its speed and changes direction upon colliding with the walls or the paddles. It also resets its position when a player scores a point.

## How to Play
- Use the 'W' and 'S' keys to control the left paddle.
- Use the UP and DOWN arrow keys to control the right paddle.
- Click anywhere on the welcome screen to start the game.


### references 
https://www.youtube.com/watch?v=iEicBgGocUA&list=PLBDInqUM5B270kU0D3TyJl_c7Z98Q2oc7&index=1




