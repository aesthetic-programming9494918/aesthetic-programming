// Ball class
class Ball {
    constructor(x, y, width, height, speed) {
      this.x = x;
      this.y = y;
      this.width = width;
      this.height = height;
      this.speed = speed;
      this.directionX = 1;
      this.directionY = 1;
    }
  
    move() {
      this.x += this.directionX * this.speed;
      this.y += this.directionY * this.speed;
  
      // Handle collisions with walls
      if (this.y >= height || this.y <= 0) {
        this.directionY *= -1;
      }
    }
  
    checkCollision(p1Y, p1Height, p2Y, p2Height, playerWidth) {
      // Collision with players
      if (
        (this.x <= 20 && this.y >= p1Y - p1Height / 2 && this.y <= p1Y + p1Height / 2) ||
        (this.x >= width - 20 && this.y >= p2Y - p2Height / 2 && this.y <= p2Y + p2Height / 2)
      ) {
        this.directionX *= -1;
      }
  
      // Score updates and reset ball position
      if (this.x <= 0) {
        p2Score++;
        this.reset();
      } else if (this.x >= width) {
        p1Score++;
        this.reset();
      }
    }
  
    reset() {
      // Reset ball position
      this.x = width / 2;
      this.y = height / 2;
    }
  
    display() {
      // Draw the ball
      fill(150, 50, 0);
      rect(this.x, this.y, this.width, this.height);
    }
  }
  
  // Global variables
  let ball;
  let p1Y = 250;
  let p2Y = 250;
  let playerWidth = 20;
  let playerHeight = 100;
  let playerSpeed = 10;
  
  let p1Score = 0;
  let p2Score = 0;
  
  // Stage variable
  let stage = 0;
  
  function setup() {
    createCanvas(900, 500);
    ball = new Ball(width / 2, height / 2, 15, 15, 5);
    textAlign(CENTER);
  }
  
  function draw() {
    if (stage === 0) {
      welcome();
    } else if (stage === 1) {
      pong();
    } else if (stage === 2) {
      p1Wins();
    } else if (stage === 3) {
      p2Wins();
    }
  }
  
  function welcome() {
    background(0);
    fill(150, 50, 0);
    textSize(200);
    text('PONG', width / 2, 200);
    textSize(30);
    text('click to start', width / 2, 350);
  }
  
  function p1Wins() {
    background(0);
    fill(150, 50, 0);
    textSize(100);
    text('PLAYER 1 WINS', width / 2, 150);
    textSize(30);
    text('REFRESH TO TRY AGAIN', width / 2, 250);
  }
  
  function p2Wins() {
    background(0);
    fill(150, 50, 0);
    textSize(100);
    text('PLAYER 2 WINS', width / 2, 200);
    textSize(30);
    text('REFRESH TO TRY AGAIN', width / 2, 250);
  }
  
  function pong() {
    // call functions
    handlePlayerInput();

    // court appearance
    background(0);
    noFill();
    stroke(150, 50, 0);
    rect(width, height, width, height); //outerboarder
    line(450, 0, 450, height);

     //set corlors
  fill (150,50,0);
  noStroke();


    // Draw players
    rect(0, p1Y, playerWidth, playerHeight);
    rect(width-20, p2Y, playerWidth, playerHeight);
    // Move and display ball
    ball.move();
    ball.checkCollision(p1Y, playerHeight, p2Y, playerHeight, playerWidth);
    ball.display();
    // Scoreboard
    textSize(20);
    text(p1Score, 400, 25);
    text(p2Score, 500, 25);
    // Check for win conditions
    if (p1Score >= 5) {
      stage = 2;
    } else if (p2Score >= 5) {
      stage = 3;
    }
  }
  
  function handlePlayerInput() {
    if (keyIsDown(87)) { // 'W' key
      p1Y -= playerSpeed;
    }
    if (keyIsDown(83)) { // 'S' key
      p1Y += playerSpeed;
    }
    if (keyIsDown(UP_ARROW)) {
      p2Y -= playerSpeed;
    }
    if (keyIsDown(DOWN_ARROW)) {
      p2Y += playerSpeed;
    }
  }
  
  function mousePressed() {
    if (stage === 0) {
      stage = 1;
    }
  }
  