let dragBarX;
let smileyColor;
let mouthCurvature;
let emojis = ['😋','😂','🥰','🥸','😎','😭','🤬','🤯']

function setup() {
  createCanvas(windowWidth, windowHeight);
  dragBarX = width / 2;
  smileyColor = color(0, 255, 0); // Initially green smiley
  mouthCurvature = 0; // 0 for neutral, positive for happy, negative for sad
  textSize(32);
  textAlign(CENTER, CENTER);
}

function draw() {
  background(255,204,0);
  drawHeading();

  for (let i = 0; i < 8; i++) { 
    let x = random(width);
    let y = random(height);
    let emoji = random(emojis);
    text(emoji, x, y);
  }
  
  // Smiley face
  fill(smileyColor);
  stroke(0);
  ellipse(width / 2, height / 2, 200, 200);
  fill(0)
  triangle(width / 2 - 40, height / 2 - 40, width / 2 - 55, height / 2 - 10, width / 2 - 25, height / 2 - 10); 
  triangle(width / 2 + 40, height / 2 - 40, width / 2 + 25, height / 2 - 10, width / 2 + 55, height / 2 - 10); 
  // Mouth
  strokeWeight(8);
  noFill();
  beginShape();
  let x1 = width / 2 - 40;
  let y1 = height / 2 + 40; //First control point of the curve
  let x2 = width / 2;
  let y2 = height / 2 + 40 + 40 * mouthCurvature; //Second control point of the curve - variable value, X is stationary, Y depends on mouthCurvature
  let x3 = width / 2 + 40;
  let y3 = height / 2 + 40;  //Third control point of the curve

  curveVertex(x1, y1);
  curveVertex(x1, y1);
  curveVertex(x2, y2);
  curveVertex(x3, y3);
  curveVertex(x3, y3); //two Vertex coordinates at the end of the curve, dont know why, but it doesnt work without;)
  endShape();

  let smileyColorRed = map(mouthCurvature, 1, -1, 0, 200);
  let smileyColorGreen = map(mouthCurvature, -1, 1, 0, 255);
  smileyColor = color(smileyColorRed, smileyColorGreen, 0); //Creates a color using the RGB components calculated above
  
  // Drag bar
  fill(230);
  stroke (0,0,230)
  strokeWeight (3)
  rect(50, height - 50, width - 100, 20, 20);
  fill(0);
  noStroke()
  ellipse(dragBarX, height - 40, 20, 20);

  if (dragBarX <= 60) {
    stroke(0);
    line(x1, y1, x3, y3);
  } //A line appears when dragged completely to the left
}

function drawHeading() {
  fill(0,0,230);
  textSize(48);
  textAlign(CENTER)
  text ("What's your current mood?", width/2, 50)

     // Draw emoji with a lifted eyebrow
  let emojiX = width / 2 + textWidth("What's your current mood?") / 2 + 40; 
  let emojiY = 50;

  fill('magenta');
  ellipse(emojiX, emojiY, 50, 50);

  fill(0);
  ellipse(emojiX - 10, emojiY - 5, 10, 20); // Left eye
  ellipse(emojiX + 10, emojiY - 5, 10, 20); // Right eye

  ellipse(emojiX -5, emojiY + 15, 10, 10); // Mouth

  noFill();
  stroke(0);
  strokeWeight(3);
  arc(emojiX - 12, emojiY - 18, 20, 20, PI, TWO_PI); // Left eyebrow
  arc(emojiX + 10, emojiY - 10, 20, 20, PI, TWO_PI); // Right eyebrow


}

function mouseDragged() {
  if (mouseY > height - 50 && mouseY < height - 30 && mouseX >= 50 && mouseX <= width - 50) {
    dragBarX = mouseX;
    
    mouthCurvature = map(dragBarX, 50, width - 50, 1, -1); // Happy to sad mouth curvature
  }
}
