# miniX02 - moody

![one example](/miniX02/Mood1.png "preview")
![another example](/miniX02/Mood2.png "preview")

You can run the program [here](https://aesthetic-programming9494918.gitlab.io/aesthetic-programming/miniX02)

You can view the code [here](https://gitlab.com/aesthetic-programming9494918/aesthetic-programming/-/tree/main/miniX02)

I had a hard time finding a concept where I could incorporate emojis, so I ended up with a classic - 'What's your mood?' 
At the top of the page, there is a heading asking you to consider what your current mood is. You can then use the page to reflect on your mood by selecting the smiley that feels most suitable for you.

In the center of the window, there is a big smiley with a drag bar underneath. The drag bar begins in the middle, and the smiley's mouth is straight. When you drag it towards the left, the smiley becomes a brighter/lighter green, and the mouth will smile more and more. When the dot on the drag bar is moved to the right, the smiley becomes gradually more red, and the mouth turns the opposite way, making it look sad.

In the background, there is a bunch of different emojis randomly appearing, but they move at such a speed that you can't really study them before they're gone again. However, they are well-known emojis, so you are able to recognize them.

I have used a lot of different shapes in the creation of the page. Last week, I got inspired by another student's miniX, which used arcs, so I decided that I had to incorporate them this week. I used them to make the eyebrows of an emoji that I placed next to the heading. The emoji is supposed to look curious.

I have tried to avoid discrimination based on gender, ethnicity, or anything else by choosing that my emoji should go from green (symbolizing positive) to red (symbolizing negative), and that it is simply a plain circle with a pair of triangular eyes and a mouth. However, a design flaw that I couldn't quite correct is that a brown color appears before it turns red when dragging the slider to the right. If it were a design that I intended to promote and use for purposes other than studies, it would definitely have been something I would fix, as there could be a risk of it being interpreted that it turns brown when heading towards the negative reaction, which absolutely has not been my intention.


References:

- https://p5js.org/reference/#/p5/arc
- https://p5js.org/reference/#/p5/curveVertex
- https://p5js.org/examples/hello-p5-simple-shapes.html
- https://p5js.org/reference/#/p5/random
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/if...else