# miniX04 - ThankYou

![example](/miniX04/DataSharing.png "preview")


You can run the program [here](https://aesthetic-programming9494918.gitlab.io/aesthetic-programming/miniX04)

You can view the code [here](https://gitlab.com/aesthetic-programming9494918/aesthetic-programming/-/tree/main/miniX04)

## My considerations behind
When I read the open call for Transmediale titled 'Capture All', my curiosity was immediately piqued. What intrigued me the most was the concept of gamification, a term I hadn't encountered before but had some inkling of its meaning. Delving deeper, I found myself familiar with its marketing implications, yet I had not given much thought to its ethical dimensions until then.

An instance of gamification that has crossed my path several times is when fashion brands seek to gather information about their customers' preferences, sizes, and styles through what appears to be a friendly questionnaire or quiz, presented as assistance in finding the perfect fit. Reflecting on my interest in gamification marketing, I felt compelled to create something that would offer a critical perspective on this approach.

As I delved into the concept of gamification, I came to understand it as the utilization of gaming elements, such as scores and competition, by companies to promote their products. While I didn't have any products to promote myself, I endeavored to craft an experience akin to a game where a camera captures the user's movements, allowing them to ‘eat’ their responses to provided questions. I found mi inspiration to the design in the PS2 game EyeToy3. To put a critical view on data capturing through gamification, I ask questions about how the user feel about datacapturing. The aim is for them to engage in something they might genuinely oppose, perhaps without even realizing it.

## The execution

The interface features a blue background, taking inspiration from the colors of Meta/Facebook. Positioned at the center of the window is a video display, incorporating an ellipse to align with your face. Within the lower-left quadrant of the ellipse, you'll find the option "yes," with "no" placed to the right. Beneath the video display, there's a prompt instructing users to position their heads within the ellipse.

Towards the top of the window, there's some text displayed. Originally, it was meant to start with "eat your answer," followed by three questions, and concluding with "Thanks for your data sharing." However, I encountered difficulties in arranging the text to display in the correct sequence; instead, it appeared to jump between phrases in a chaotic manner. Despite many challenges I succeed to get the text to switch when I opened my mouth, but only if my head remained still within the ellipse, because it is based on the position of a point on the lower lip.

In summary, the interface doesn't function as intended. Nonetheless, I hope this description provides some insight into my intended concept. 