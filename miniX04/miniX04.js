let faceTracker;
let video;
let facePositions;
let questions = ["eat your answer", 
"Do you believe that the benefits of data capturing, such as personalized recommendations, outweigh the potential risks to privacy?", 
"Do you think individuals should have more control over how their data is captured and used by companies?", 
"Would you be willing to share more personal information if it meant receiving better services or products tailored to your needs?", 
"Thanks for your datasharing"];
let currentQuestionIndex = 0;
let previousMouthPositionY;
let currentMouthPositionY;
let smileThreshold = 5;
let centerMouthPoint = 55;
let expressionHasBeenChanged = false;

function setup() {
  createCanvas(windowWidth, windowHeight);
  video = createCapture(VIDEO);
  video.size(700, 500);
  video.hide();
  faceTracker = new clm.tracker();
  faceTracker.init();
  faceTracker.start(video.elt);
}

function draw() {
  background(50,100,255);

  // flips the video
  push();
  translate(video.width, 0);
  scale(-1, 1);
  image(video, -windowWidth/4, 50, 700, 500);
  pop(); 
 
  // draw UI elements 
  noFill();
  ellipse(725, 250, 250, 350)
  fill(0);
  textSize(20);
  text('place your head in the circle', 725, 560)
  text('yes',675,350)
  text('no',775,350)
  textAlign(CENTER, TOP);
  text(questions[currentQuestionIndex], width / 2, 10);

  // tracks face positions
  facePositions = faceTracker.getCurrentPosition();
  handleExpressionChange()

  if (facePositions.length) {
    currentMouthPositionY = facePositions[centerMouthPoint][1]
    if (frameCount % 40 == 0) {
      updatePreviousMouthPosition()
    }
    expressionHasBeenChanged = currentMouthPositionY - previousMouthPositionY > smileThreshold
  }
}

function updatePreviousMouthPosition() {
  previousMouthPositionY = facePositions[centerMouthPoint][1]
}

function handleExpressionChange() {
  if (expressionHasBeenChanged) {
    currentQuestionIndex++;
    if (currentQuestionIndex >= questions.length) {
      currentQuestionIndex = 0; // Reset back to the first question
    }
  }
}
